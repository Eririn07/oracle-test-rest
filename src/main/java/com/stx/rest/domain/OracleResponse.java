package com.stx.rest.domain;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class OracleResponse {
    public SampleServiceRs sampleservicers;
}
