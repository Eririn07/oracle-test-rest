package com.stx.rest.domain;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class sampleservicerq {
    private String service_id;
    private String order_type;
    private String type;
    private String trx_id;
}
