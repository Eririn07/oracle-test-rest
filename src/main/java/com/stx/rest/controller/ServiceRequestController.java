package com.stx.rest.controller;

import com.stx.rest.domain.OracleRequest;
import com.stx.rest.domain.OracleResponse;
import com.stx.rest.domain.sampleservicerq;
import com.stx.rest.domain.SampleServiceRs;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/external/services/rest")
@Slf4j
public class ServiceRequestController {

    @PostMapping("/sample-service")
    public ResponseEntity<OracleResponse> GetRest(@RequestBody OracleRequest request){
        log.info("REQUEST POST: {}", request);
        OracleResponse response = new OracleResponse();
        SampleServiceRs rs = new SampleServiceRs();

        rs.setError_code("0000");
        rs.setError_msg("Success");
        rs.setTrx_id(request.getSampleservicerq().getTrx_id());

        response.setSampleservicers(rs);

        return ResponseEntity.ok(response);
    }
}
